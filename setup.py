from setuptools import setup, find_packages

setup(name='django-scaffold',
      version='1.0',
      packages=find_packages(),
      scripts=['manage.py'])